# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :orderId
      t.string :status

      t.timestamps
    end
  end
end

# "orders": [
#         {
#             "orderId": 1122,
#             "status": "Pending",
#             "dateCreated": "2018-05-09 10:59",
#             "items": [
#                 {
#                     "orderId": 1122,
#                     "productId": 1,
#                     "quantity": 4,
#                     "costPerItem": 10.45
#                 },
#                 {
#                     "orderId": 1122,
#                     "productId": 2,
#                     "quantity": 7,
#                     "costPerItem": 20.95
#                 }
#             ]
#         },
