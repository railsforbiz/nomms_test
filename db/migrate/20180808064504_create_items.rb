# frozen_string_literal: true

class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.integer :orderId
      t.integer :productId
      t.integer :quantity
      t.decimal :costPerItem, precision: 5, scale: 2, default: 0

      t.timestamps
    end
  end
end

# {
#                     "orderId": 1122,
#                     "productId": 1,
#                     "quantity": 4,
#                     "costPerItem": 10.45
