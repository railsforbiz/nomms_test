# frozen_string_literal: true

class CreateFulfils < ActiveRecord::Migration[5.2]
  def change
    create_table :fulfils do |t|
      t.string :orderId

      t.timestamps
    end
  end
end
