# frozen_string_literal: true

class ChangeOrderIdToString < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :orderId, :string
  end
end
