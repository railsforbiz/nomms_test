# frozen_string_literal: true

class CreatePurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_orders do |t|
      t.integer :product_id
      t.integer :quantity
      t.string :description

      t.timestamps
    end
  end
end
