# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :description
      t.integer :quantityOnHand
      t.integer :reorderThreshold
      t.integer :reorderAmount
      t.integer :deliveryLeadTime

      t.timestamps
    end
  end
end

# "productId": 1,
#            "description": "Small Widget",
#            "quantityOnHand": 50,
#            "reorderThreshold": 10,
#            "reorderAmount": 50,
#            "deliveryLeadTime": 5
