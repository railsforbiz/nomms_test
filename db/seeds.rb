# frozen_string_literal: true

# require 'database_cleaner'

# DatabaseCleaner.strategy = :truncation
# DatabaseCleaner.clean
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

PurchaseOrder.delete_all
Order.delete_all
Product.delete_all
Item.delete_all
Fulfil.delete_all

# PurchaseOrder.create!(product_id: 1, quantity: 20, description: 'SEED TEST')

# create_table "purchase_orders", force: :cascade do |t|
#     t.integer "product_id"
#     t.integer "quantity"
#     t.string "description"
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#   end
