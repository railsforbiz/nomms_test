# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Fulfil, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"

  context 'orderId should be provided' do
    it 'ensures orderId presence is true' do
      fulfil = Fulfil.new(orderId: '2122').save
      expect(fulfil).to eq(true)
    end

    it 'orderId not provided returns false' do
      fulfil = Fulfil.new(orderId: '').save
      expect(fulfil).to eq(false)
    end
  end

  context 'orderId should be processed against existing pending orders' do
    before :each do
      fulfilment = Fulfil.new(orderId: '1122').save
    end
    it 'ensures to fire service inventory_logic.rb successfully' do
      # fulfilment = Fulfil.new(orderId: '1122').save
      # fulfilment = Fulfil.new(orderId: '1123').save
      # fulfilment = Fulfil.create!(orderId: '1122')
      # call inventory logic and pass orderId to engage logic
      fulfilment = Fulfil.find_by_orderId('1122')

      # pass test data
      # data = File.read('data.json')
      url = ENV['datajson']

      # data1 = JSON.parse(url)

      respa = Net::HTTP.get_response(URI.parse(url))

      data1 = JSON.parse(respa.body)
      orders = data1['orders']
      products = data1['products']

      # WarehouseLogic::InventoryLogic.new(orders: orders, products: products, fulfilment: @fulfil).call
      DataEntry::PassData.new(products: products, orders: orders).call
      # test order exists
      # order = Order.find_by_orderId(fulfilment.orderId)
      # expect(order).to eq(true)
      expect(orders).not_to be_empty
      expect(products).not_to be_empty
      expect(fulfilment.orderId).to eq('1122')
    end
  end
end
