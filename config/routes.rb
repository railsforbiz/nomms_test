# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      namespace :warehouse do
        resources :fulfilment
        resources :products
        resources :orders
        resources :purchases
        resources :items
      end
    end
  end
end
