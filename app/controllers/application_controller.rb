# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # skip_before_action :verify_authenticity_token, if: :json_request?
  # protect_from_forgery with: :null_session
  # protect_from_forgery with: :null_session, if: Proc.new {|c| c.request.format.json? }
  protect_from_forgery unless: -> { request.format.json? }
end
