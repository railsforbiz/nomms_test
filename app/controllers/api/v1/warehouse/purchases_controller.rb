# frozen_string_literal: true

module Api
  class V1::Warehouse::PurchasesController < ApplicationController
    def index
      purchaseorders = PurchaseOrder.limit(5).order('id DESC')

      render json: purchaseorders, status: :ok
    end
end
end
