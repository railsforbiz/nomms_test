# frozen_string_literal: true

require 'net/http'
require 'net/https'
require 'uri'

module Api
  class V1::Warehouse::FulfilmentController < ApplicationController
    skip_before_action :verify_authenticity_token, if: :json_request?

    def index
      # data = File.read('data.json')
      url = ENV['datajson']

      # data1 = JSON.parse(url)

      respa = Net::HTTP.get_response(URI.parse(url))

      data1 = JSON.parse(respa.body)
      orders = data1['orders']
      products = data1['products']

      # WarehouseLogic::InventoryLogic.new(orders: orders, products: products, fulfilment: @fulfil).call
      DataEntry::PassData.new(products: products, orders: orders).call

      render json: data1, status: :ok
    end

    # this will POST and submit, need to call out business logic at services to handle conditions regarding stock levels
    def create
      @fulfil = Fulfil.new(ful_params)
      if @fulfil.save
        url = ENV['datajson']
        respa = Net::HTTP.get_response(URI.parse(url))

        data1 = JSON.parse(respa.body)
        orders = data1['orders']
        products = data1['products']

      end

      # manipulate responses based on certain inventory conditions

      # render this format if order failed
      # {
      #       â€œunfulfillableâ€: [1123,1125]
      # }
      WarehouseLogic::InventoryLogic.new(orders: orders, products: products, fulfilment: @fulfil).call
      order = Order.find_by_orderId(@fulfil.orderId)

      if order.status == 'UNFULFILLED'
        render json: { status: "#{order.orderId} Unfulfilable, Inventory low." }
      else
        render json: @fulfil, status: :created
      end
             end

    private

    def ful_params
      params.permit(:orderId)
     end
    end
end

# t.string "orderId"
#   t.string "status"