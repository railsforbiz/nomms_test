# frozen_string_literal: true

require 'net/http'
require 'net/https'
require 'uri'

module Api
  class V1::Warehouse::ProductsController < ApplicationController
    def index
      # # data = File.read('data.json')
      # url = ENV['datajson']

      # # data1 = JSON.parse(url)

      # respa = Net::HTTP.get_response(URI.parse(url))

      # data1 = JSON.parse(respa.body)
      # products = data1['products']

      products = Product.all

      render json: products, status: :ok
      # @data_json = JSON.parse(json)
    end

    # this will POST and submit, need to call out business logic at services to handle conditions regarding stock levels
    def create; end
    end
end
