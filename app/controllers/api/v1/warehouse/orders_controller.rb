# frozen_string_literal: true

require 'net/http'
require 'net/https'
require 'uri'

module Api
  class V1::Warehouse::OrdersController < ApplicationController
    def index
      # data = File.read('data.json')
      # url = ENV['datajson']

      # # data1 = JSON.parse(url)

      # respa = Net::HTTP.get_response(URI.parse(url))

      # data1 = JSON.parse(respa.body)
      # orders = data1['orders']
      orders = Order.limit(10).order('id desc')
      render json: orders, status: :ok
      # @data_json = JSON.parse(json)
    end

    # this will POST and submit, need to call out business logic at services to handle conditions regarding stock levels
    def create; end
    end

  def show
    url = ENV['datajson']

    # data1 = JSON.parse(url)

    respa = Net::HTTP.get_response(URI.parse(url))

    data1 = JSON.parse(respa.body)
    i = param[:id]
    order = data1['orders'][i]

    # order = (order.find { |u| u['orderId'] == @orderId })['order']

    # order = orders[params[:id]]
    render json: order, status: :ok
  end
end
