# frozen_string_literal: true

class Fulfil < ApplicationRecord
  validates :orderId, presence: true
end
