# frozen_string_literal: true

class WarehouseLogic::PurchaseLogic
  def initialize(params)
    @purchaseorder = params[:purchaseorder]
    @products = params[:products]
   end

  def call
    process_po
  end

  private

  def process_po
    # proceed with purchase order

    pid = @purchaseorder.id

    qty = @purchaseorder.reorderAmount

    desc = 'Auto-restock'

    po = PurchaseOrder.create!(
      product_id: pid,
      quantity: qty,
      description: desc
    )

    # simulated purchase order, restock products

    product = Product.find_by_id(po.product_id)

    stock = product.quantityOnHand.to_f + po.quantity.to_f

    product.quantityOnHand = stock.to_i
    product.save!

    #  t.string "description"
    # t.integer "quantityOnHand"
    # t.integer "reorderThreshold"
    # t.integer "reorderAmount"
    # t.integer "deliveryLeadTime"
  end
end

# t.integer "product_id"
#   t.integer "quantity"
#   t.string "description"
