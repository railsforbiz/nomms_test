# frozen_string_literal: true

# inventory_logic.rb

# this will serve as the logic conditions on order fulfilment

# condition 1
#  verify if an adequate stock is in inventory before processing orders

# condition 2
#   provide an error gateway to highlight failed orders

# condition 3
#  flag items for re-order

class WarehouseLogic::InventoryLogic
  def initialize(params)
    @fulfilment = params[:fulfilment]
    @products = params[:products]
    @orders = params[:orders]
   end

  def call
    pass_data

    inventory_logic_gate
  end

  private

  def pass_data
    DataEntry::PassData.new(products: @products, orders: @orders).call
  end

  def inventory_logic_gate
    # pass fulfil variable post

    # pull out matching orders
    order = Order.find_by_orderId(@fulfilment.orderId)
    cart = Item.where(orderId: @fulfilment.orderId)

    # pull out stock values in products

    # check all items first do not process yet
    @flag = 0
    cart.each do |ca|
      product = Product.find_by_id(ca.productId.to_i)

      # verify if stock good to go

      @flag += 1 if product.quantityOnHand >= ca.quantity
    end
    if @flag == cart.count

      cart.each do |ca|
        product = Product.find_by_id(ca.productId.to_i)

        # verify if stock good to go
        next unless product.quantityOnHand >= ca.quantity
        ans = product.quantityOnHand.to_f - ca.quantity.to_f
        product.quantityOnHand = ans.to_i
        product.save!
        # elsif product.quantityOnHand <= product.reorderThreshold || product.quantityOnHand <= ca.quantity
      end
      order.status = 'FULFILLED'
      order.save!

    elsif @flag < cart.count
      cart.each do |ca|
        product = Product.find_by_id(ca.productId.to_i)
        # do not process order and trigger purchase order

        WarehouseLogic::PurchaseLogic.new(purchaseorder: product, product: @products).call if product.quantityOnHand <= product.reorderThreshold
      end
      order.status = 'UNFULLFILLED'
      order.save!
      # needs an option to reflect unprocesed orders

    end

    # item
    #   t.integer "orderId"
    # t.integer "productId"
    # t.integer "quantity"
    # t.decimal "costPerItem",

    # product
    # t.string "description"
    #   t.integer "quantityOnHand"
    #   t.integer "reorderThreshold"
    #   t.integer "reorderAmount"
    #   t.integer "deliveryLeadTime"

    # read order conditions
    # verify adequate stock on hand
    # check and verify order details

    # pull out product stock from db based from imported json

    # product_stock1 = @products[0]['quantityOnHand']
    # product_stock2 = @products[1]['quantityOnHand']
    # product_stock3 = @products[2]['quantityOnHand']

    # orders

    # check if there is enough stock,

    # place order if stock above order value

    # orderid = @fulfilment.orderId.to_i

    # find_order = (@orders.find { |u| u['orderId'] == orderid })
    # product_ordered1 = find_order['items'][0]
    # product_ordered2 = find_order['items'][1]
    # product_ordered3 = find_order['items'][2]

    # find_product = (@products).find {|u| u['productId'] = product_ordered1}

    # redo thiss

    # if find_product['quantityOnHand'] > product_ordered1['quantity']
    #     item = Item.create!(
    #         orderId: product_ordered1['orderId'],
    #         productId: product_ordered1['productId'],
    #         costPerItem: product_ordered1['costPerItem'],
    #         quantity: product_ordered1['quantity']
    #       )
    # end

    # process reorder if stock below threshold

    # if product_stock1 <= @products[0]['reorderThreshold']
    #   # process purchase order

    #   WarehouseLogic::PurchaseLogic.new(purchaseorder: @products[0], products: @products).call

    # elsif product_stock2 <= @products[1]['reorderThreshold']

    #   WarehouseLogic::PurchaseLogic.new(purchaseorder: @products[1], products: @products).call

    # elsif product_stock3 <= @products[2]['reorderThreshold']

    #   WarehouseLogic::PurchaseLogic.new(purchaseorder: @products[2], products: @products).call

    # end
  end
end