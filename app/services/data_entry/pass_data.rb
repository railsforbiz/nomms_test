# frozen_string_literal: true

class DataEntry::PassData
  def initialize(params)
    @products = params[:products]
    @orders = params[:orders]
   end

  def call
    pass_products
    pass_orders
  end

  private

  def pass_orders
    order_empty_check = Order.count
    if order_empty_check == 0
      @orders.each do |o|
        # order_id_check = Order.find_by_orderId(o['orderId'].to_i)
        # if order_id_check == nil
        # t.string "orderId"
        # t.string "status"
        order = Order.create!(
          orderId: o['orderId'],
          status: 'PENDING'
        )
        # t.integer "orderId"
        # t.integer "productId"
        # t.integer "quantity"
        # t.decimal "costPerItem",

        # end
        o['items'].each do |i|
          item = Item.create!(
            orderId: i['orderId'].to_i,
            productId: i['productId'].to_i,
            quantity: i['quantity'].to_i,
            costPerItem: i['costPerItem'].to_f
          )
        end
      end

    end
      end

  def pass_products
    # pass data to model

    #  t.string "description"
    # t.integer "quantityOnHand"
    # t.integer "reorderThreshold"
    # t.integer "reorderAmount"
    # t.integer "deliveryLeadTime"

    # products defaults
    product_empty_check = Product.count
    if product_empty_check == 0
      @products.each do |p|
        product_model = Product.find_by_id(p['productId'].to_i)
        next unless product_model.nil?
        p_entry = Product.create!(
          description: p['description'],
          quantityOnHand: p['quantityOnHand'].to_i,
          reorderThreshold: p['reorderThreshold'].to_i,
          reorderAmount: p['reorderAmount'].to_f,
          deliveryLeadTime: p['deliveryLeadTime'].to_i
        )
      end
    end
  end
end
