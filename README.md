# README


Ruby: 2.5.1 
Rails: 5.2.1
Rspec 3.8



Conditions

1. Read from unmodified json file
2. create api/v1/warehouse/fulfilment endpoint and others as needed for demo purpose.
3. services to handle warehouse logic to be called from interaction of endpoint api/v1/warehouse/fullfillment via POST
4. Persistence not required
5. Authentication not required





Approach


1. Had some problems with loading the data.json via File.read, had to load the json file via url served under public/ folder
2. Routing of open/unauthenticated endpoint 'api/v1/warehouse/fulfilment' via controller approach without gems such as grape, swagger, doorkeeper, etc
3. Create PurchaseOrder model to dump simulated PurchaseOrder via sqlite3 to easily verify Purchase Orders are triggered.
4. Added figaro for application Environment to provide variable central swithhub, will plan to toggle to url location of data.json so i can deploy to a private server for demo purposes and via git clone local server deployment, url switch can be found at config/application.yml
5. Import data.json and pass to Active Models, for individual data manipulation and retrieval
6. Simulated Auto-reorder based on reorder threshold and restock logics
7. Persistence is in effect, execute rails db:seed to reset db to blank, triggers data.json import to reset data
8. Inventory Logic Math is active, if stock quantity is greater than order quantity, order will be processed and deducted from stock onhand value. If stock is low, order will not be processed, and shall trigger auto order based on treshhold and restock quantity. once purchased order submitted, stock on hand will be updated, by adding new qty from restock quantity. and continuesly auto-reorder and restock as needed.
9. Improving Inventory and Purchase Order Arithmetic
10. Implementing Rspec Testing, using guard-rspec for listening
11. Since we used database for data storage, data imported from data.json is loaded into rails active models. Data manipulation is then connected via rails active models. For real-world application, with transition with legacy systems using json, for data import/transfers. Rails scripts can automate the data importation into active models. New data will then be handled by rails active models.


Challenges Encountered
1. Json array, manipulation, query & find interaction
2. Rspec config regarding require(relative path vs absolute path).
2.a Running 'bundle exec rspec' instead of 'rspec' on the terminal runs rspec with no errors. Due to rspec-expectations version mismatch.
3. Found an issue, with sqlite3 when using database cleaning, via seed.rb with Model.delete_all.
3.1 Solution is to delete development.sqlite3 and run "rails db:migrate"


Postman
for endpoints interaction test

Rspec
for testing




API Endpoints

GET api/v1/warehouse/fulfilment - view data.json 
POST api/v1/warehouse/fulfilment - post "orderID"


GET api/v1/warehouse/products - To view products/details/stocks
GET api/v1/warehouse/orders - To view orders, status, etc
GET api/v1/warehouse/purchases - To view purchase ordered





Live Demo Server

http://dev1.railsfor.biz:3002 not yet deployed
dev1 is running Ruby 2.4.1 need updates
dev2 is running Ruby 2.4.1 need updates


For Local Server

1. git clone https://saisake@bitbucket.org/railsforbiz/nomms_test.git
2. navigate to nomms_test/
3. bundle install && rails db:seed &&rails s

